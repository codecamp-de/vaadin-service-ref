# Serializable Service References for Vaadin Flow 

This mini add-on provides serializable service references. Currently Spring Boot is supported. But there's an untested implementation for CDI included as well; contact me if you're interested.


## How does it work?

Vaadin stores all UIs in their respective HTTP session. If you want to be able to persist your sessions (so they survive a server restart or can be migrated to a different server), everything in these UIs needs to be serializable. While dependency injection provides easy and convenient access to your services, these services aren't and shouldn't be serializable. You could always use static methods to access your services, but you would loose the convenience of DI. This add-on solves this by injecting serializable service references instead of services directly.


## Example

Inject your service like this:

```java
// Spring
@Autowired
private ServiceRef<MyService> myService;

// CDI
@Inject
private ServiceRef<MyService> myService;
```

And then use it like this:

```java
myService.get().doStuff();
```
