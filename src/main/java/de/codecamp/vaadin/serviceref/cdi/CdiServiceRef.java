package de.codecamp.vaadin.serviceref.cdi;


import de.codecamp.vaadin.serviceref.ServiceRef;
import jakarta.enterprise.inject.spi.CDI;
import java.lang.annotation.Annotation;
import java.util.Objects;


/**
 * {@link ServiceRef} implementation for CDI.
 *
 * @param <S>
 *          the service type
 */
public final class CdiServiceRef<S>
  implements
    ServiceRef<S>
{

  /**
   * the service type
   */
  private Class<S> serviceType;

  /**
   * the (optional) qualifiers for the service
   */
  private Annotation[] qualifiers;

  /**
   * transient reference for the service
   */
  private transient S service;


  private CdiServiceRef(Class<S> serviceType)
  {
    this(serviceType, (Annotation[]) null);
  }

  private CdiServiceRef(Class<S> serviceType, Annotation... qualifiers)
  {
    Objects.requireNonNull(serviceType, "serviceType must not be null");

    this.serviceType = serviceType;
    this.qualifiers = qualifiers;
  }


  @Override
  public synchronized S get()
  {
    if (service == null)
    {
      service = get(serviceType, qualifiers);
    }
    return service;
  }


  public static <S> ServiceRef<S> of(Class<S> serviceType)
  {
    return new CdiServiceRef<>(serviceType);
  }

  public static <S> ServiceRef<S> of(Class<S> serviceType, Annotation... qualifiers)
  {
    return new CdiServiceRef<>(serviceType, qualifiers);
  }


  /**
   * Looks up and returns a service instance of the given type from the application context.
   *
   * @param <T>
   *          the type of the service
   * @param serviceType
   *          the type of the service
   * @return a service instance
   */
  public static <T> T get(Class<T> serviceType)
  {
    return get(serviceType, (Annotation[]) null);
  }

  /**
   * Looks up and returns a service instance of the given type and with the given qualifier from the
   * application context.
   *
   * @param <T>
   *          the type of the service
   * @param serviceType
   *          the type of the service
   * @param qualifiers
   *          the qualifiers
   * @return a service instance
   */
  public static <T> T get(Class<T> serviceType, Annotation... qualifiers)
  {
    if (qualifiers != null)
      return CDI.current().select(serviceType, qualifiers).get();
    else
      return CDI.current().select(serviceType).get();
  }

}
