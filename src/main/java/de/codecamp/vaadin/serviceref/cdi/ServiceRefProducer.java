package de.codecamp.vaadin.serviceref.cdi;


import de.codecamp.vaadin.serviceref.ServiceRef;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.context.Dependent;
import jakarta.enterprise.inject.Produces;
import jakarta.enterprise.inject.spi.InjectionPoint;
import java.lang.annotation.Annotation;
import java.lang.reflect.ParameterizedType;


@ApplicationScoped
public class ServiceRefProducer
{

  @Produces
  @Dependent
  public <S> ServiceRef<S> serviceRef(InjectionPoint injectionPoint)
  {
    return createServiceRefFor(injectionPoint);
  }


  /**
   * Creates a {@link ServiceRef} based on the given {@link InjectionPoint}.
   *
   * @param <S>
   *          the service type
   * @param injectionPoint
   *          the injection point
   * @return a new {@link ServiceRef}
   */
  public static <S> ServiceRef<S> createServiceRefFor(InjectionPoint injectionPoint)
  {
    ParameterizedType type = (ParameterizedType) injectionPoint.getType();
    @SuppressWarnings("unchecked")
    Class<S> serviceType = (Class<S>) type.getActualTypeArguments()[0];
    return CdiServiceRef.of(serviceType, injectionPoint.getQualifiers()
        .toArray(new Annotation[injectionPoint.getQualifiers().size()]));
  }

}
