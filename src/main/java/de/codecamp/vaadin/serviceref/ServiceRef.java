package de.codecamp.vaadin.serviceref;


import java.io.Serializable;


/**
 * Serializable, late-binding service reference.
 *
 * @param <S>
 *          the service type
 */
public interface ServiceRef<S>
  extends
    Serializable
{

  /**
   * Returns the service instance, looking it up if necessary.
   *
   * @return the service instance
   */
  S get();

}
