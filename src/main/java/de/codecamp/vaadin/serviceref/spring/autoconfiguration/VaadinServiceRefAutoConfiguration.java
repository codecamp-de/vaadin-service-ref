package de.codecamp.vaadin.serviceref.spring.autoconfiguration;


import de.codecamp.vaadin.serviceref.ServiceRef;
import de.codecamp.vaadin.serviceref.spring.ServiceRefFactoryBean;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.InjectionPoint;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Scope;


@AutoConfiguration
public class VaadinServiceRefAutoConfiguration
{

  @Bean
  @Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
  @Primary
  public <S> FactoryBean<ServiceRef<S>> vaadinServiceRefFactory(InjectionPoint injectionPoint)
  {
    return new ServiceRefFactoryBean<>(injectionPoint);
  }

}
