package de.codecamp.vaadin.serviceref.spring;


import de.codecamp.vaadin.serviceref.ServiceRef;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.InjectionPoint;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.ResolvableType;
import org.springframework.core.annotation.AnnotationUtils;


/**
 * A {@link FactoryBean} to create {@link ServiceRef} based on the injection point.
 *
 * @param <S>
 *          the service type
 */
public class ServiceRefFactoryBean<S>
  implements
    FactoryBean<ServiceRef<S>>
{

  private final InjectionPoint injectionPoint;


  public ServiceRefFactoryBean(InjectionPoint injectionPoint)
  {
    this.injectionPoint = injectionPoint;
  }


  @Override
  public Class<?> getObjectType()
  {
    return ServiceRef.class;
  }

  @Override
  public boolean isSingleton()
  {
    return false;
  }

  @Override
  public ServiceRef<S> getObject()
    throws Exception
  {
    ResolvableType serviceRefType;
    if (injectionPoint.getField() != null)
      serviceRefType = ResolvableType.forField(injectionPoint.getField());
    else
      serviceRefType = ResolvableType.forMethodParameter(injectionPoint.getMethodParameter());

    @SuppressWarnings("unchecked")
    Class<S> serviceType = (Class<S>) serviceRefType.getGeneric(0).resolve();

    String qualifier = null;
    Qualifier qualifierAt =
        AnnotationUtils.getAnnotation(injectionPoint.getAnnotatedElement(), Qualifier.class);
    if (qualifierAt != null)
      qualifier = qualifierAt.value();

    return SpringServiceRef.of(serviceType, qualifier);
  }

}
