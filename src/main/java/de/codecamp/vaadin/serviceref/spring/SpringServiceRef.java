package de.codecamp.vaadin.serviceref.spring;


import com.vaadin.flow.server.VaadinServlet;
import de.codecamp.vaadin.serviceref.ServiceRef;
import java.util.Objects;
import org.springframework.beans.factory.annotation.BeanFactoryAnnotationUtils;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;


/**
 * {@link ServiceRef} implementation for Spring.
 *
 * @param <S>
 *          the service type
 */
public final class SpringServiceRef<S>
  implements
    ServiceRef<S>
{

  /**
   * the service type
   */
  private Class<S> serviceType;

  /**
   * an (optional) qualifier for the service
   */
  private String qualifier;

  /**
   * transient reference for the service
   */
  private transient S service;


  private SpringServiceRef(Class<S> serviceType)
  {
    this(serviceType, null);
  }

  private SpringServiceRef(Class<S> serviceType, String qualifier)
  {
    Objects.requireNonNull(serviceType, "serviceType must not be null");

    this.serviceType = serviceType;
    this.qualifier = qualifier;
  }


  /**
   * Returns the service instance, looking it up from the application context if necessary.
   *
   * @return the service instance
   */
  @Override
  public synchronized S get()
  {
    if (service == null)
    {
      if (qualifier == null)
        service = get(serviceType);
      else
        service = get(serviceType, qualifier);
    }
    return service;
  }


  public static <S> ServiceRef<S> of(Class<S> serviceType)
  {
    return new SpringServiceRef<>(serviceType);
  }

  public static <S> ServiceRef<S> of(Class<S> serviceType, String qualifier)
  {
    return new SpringServiceRef<>(serviceType, qualifier);
  }


  /**
   * Looks up and returns a service instance of the given type from the application context.
   *
   * @param <T>
   *          the type of the service
   * @param serviceType
   *          the type of the service
   * @return a service instance
   */
  public static <T> T get(Class<T> serviceType)
  {
    return getApplicationContext().getBean(serviceType);
  }

  /**
   * Looks up and returns a service instance of the given type and with the given qualifier from the
   * application context.
   *
   * @param <T>
   *          the type of the service
   * @param serviceType
   *          the type of the service
   * @param qualifier
   *          the qualifier
   * @return a service instance
   */
  public static <T> T get(Class<T> serviceType, String qualifier)
  {
    return BeanFactoryAnnotationUtils.qualifiedBeanOfType(
        getApplicationContext().getAutowireCapableBeanFactory(), serviceType, qualifier);
  }

  private static WebApplicationContext getApplicationContext()
  {
    return WebApplicationContextUtils
        .getWebApplicationContext(VaadinServlet.getCurrent().getServletContext());
  }

}
